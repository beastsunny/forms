const slider = document.getElementById('userSlider')
const Plancard1 = document.getElementById('planCard1')
const Plancard2 = document.getElementById('planCard2')
const Plancard3 = document.getElementById('planCard3')

const resultsContainer = document.getElementById('results');
const loadMoreButton = document.getElementById('loadMore');
let page = 1; // Initial page number
const perPage = 10; // Number of results per page

function highlightPlan() {
       const sliderValue = slider.value

      Plancard1.classList.remove('highlighted-plan');
      Plancard2.classList.remove('highlighted-plan');
      Plancard3.classList.remove('highlighted-plan');



       if (sliderValue >= 0 && sliderValue < 10) {
        Plancard1.classList.add('highlighted-plan');
      } else if (sliderValue >= 10 && sliderValue < 20) {
        Plancard2.classList.add('highlighted-plan');
      } else {
        Plancard3.classList.add('highlighted-plan');
      }


    
    }

    slider.addEventListener('input', highlightPlan);


    async function fetchData() {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${perPage}`);
        const data = await response.json();
        data.forEach(post => {
            const postElement = document.createElement('div');
            postElement.classList.add('post');
            postElement.innerHTML = `<h2>${post.title}</h2><p>${post.body}</p>`;
            resultsContainer.appendChild(postElement);
        });
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Initial data fetch
fetchData();

// Function to load more data
function loadMore() {
    page++; // Increment page number
    fetchData();
}

// Event listener for the "Load More" button
loadMoreButton.addEventListener('click', loadMore);

// Event listener to check if user has scrolled to the end
window.addEventListener('scroll', () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        loadMore();
    }
});